(defproject jira-cli "0.1.10"
  :description "Tools for doing JIRA, bitbucket, etc operations from CLI"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 ;; [com.google.oauth-client/google-oauth-client "1.22.0"] ;; will need in future once exampleclient dep is removed
                 [camel-snake-kebab "0.4.1"]
                 [cheshire "5.10.0"]
                 [circleci/clj-yaml "0.6.0"]
                 [cli4clj "1.7.6"]
                 [clj-http "3.10.3"]
                 [clj-time "0.15.2"]
                 ;;[com.atlassian.oauth.v1/OAuthTutorialClient "1.3"] ;; only for debug and generating initial keys
                 [com.google.api.client/google-api-client-auth-oauth "1.2.3-alpha"]
                 [com.google.http-client/google-http-client "1.11.0-beta"]
                 [com.fzakaria/slf4j-timbre "0.3.20"]
                 [com.taoensso/timbre "4.10.0" :exclusions [org.clojure/tools.reader]] ;; NB DO NOT EVER UPGRADE 5.1 IS BROKEN
                 [me.raynes/conch "0.8.0" :exclusions [org.flatland/useful]]
                 [mount "0.1.16"]
                 [oauth-clj "0.1.16"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [org.clojure/tools.reader "1.3.3" :exclusions [org.clojure/clojure]] ;; settle the dep wars
                 [org.clojure/tools.trace "0.7.10"]
                 [utilza "0.1.110" :exclusions [org.clojure/clojure]]
                 ]
  :main ^:skip-aot jira-cli.core
  :target-path "target/%s"
  :aliases {"junit" ["with-profile" "test" "do" "test-out" "junit" "junit.xml"]
            "binary" ["with-profile" "uberjar" "bin"]
            "revision" ["run" "-m" "jira-cli.log/build-version"]
            "coverage" ["with-profile" "test" "do" "cloverage"]}
  :profiles {:uberjar {:aot :all
                       :dependencies []
                       :bin {:name "jira-cli"
                             :bin-path "target/uberjar"}
                       :plugins [[lein-bin "0.3.5"]]
                       :uberjar-name "jira-cli.jar"}
             :dev {:plugins [[lein-difftest "2.0.0"]
                              [lein-test-out "0.3.1"]
                              [org.clojure/test.check "1.1.0"]
                              [lein-cloverage "1.2.1"]]
                    :test-paths ["test" "src"]}
             })

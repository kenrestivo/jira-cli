(ns jira-cli.cli
  (:require
   (cli4clj [cli :as cli])
   (clojure [pprint :as pprint])
   [clj-http.client :as client]
   [clojure.edn :as edn]
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [jira-cli.commands :as com]
   [jira-cli.conf :as conf]
   [jira-cli.encode :as enc]
   [jira-cli.log :as jlog]
   [jira-cli.net.actions :as actions]
   [jira-cli.net.queries :as queries]
   [jira-cli.nrepl :as nrepl] ; force!
   [jira-cli.parse :as parse]
   [jira-cli.utils :as utils]
   [mount.core :as mount]
   [taoensso.timbre :as log]
   [utilza.log :as ulog]
   [utilza.misc :as umisc]
   [utilza.repl :as urepl]
   ))




(defn cli
  [_]
  (cli/start-cli {:prompt-string "jira> "
                  :cmds {:test {:fn #(println "This is a test.")
                                :short-info "Test Command"
                                :long-info "Prints a test message to stdout."}
                         :everything-i-have-touched  {:short-info "Get everything i have touched"
                                                      :long-info "prints them on org mode"
                                                      :fn com/print-everything-i-have-touched!}
                         :my-open-issues  {:short-info "Get my open issues"
                                           :long-info "prints them on org mode"
                                           :fn com/print-my-open-issues!}
                         :add-labels {:short-info "Add labels to tickets"
                                      :long-info "Takes a ticket number, and any number of labels"
                                      :fn com/add-labels!}
                         :issues-by-label {:short-info "Print all issues by label"
                                           :long-info "Takes a label."
                                           :fn com/print-tickets-by-label!}
                         :add-comment {:short-info "Add comment to tickets"
                                       :long-info "Takes a ticket number, and the comment"
                                       :fn com/add-comment!}
                         :link-tickets {:short-info "Link 2 tickets"
                                        :long-info "Takes a ticket number, ticket number to link to, and reason"
                                        :fn com/link-tickets!}
                         :get-comments {:short-info "Get comments about a ticket"
                                        :long-info "takes a ticket number"
                                        :fn com/print-comments!}
                         :set-log-level {:short-info "Set the log level"
                                         :long-info "Takes level: trace, debug, info, warn, error"
                                         :fn com/set-log-level!}
                         :get-ticket {:short-info "Get info about a ticket"
                                      :long-info "takes a ticket number"
                                      :fn com/print-ticket!}
                         :browse-ticket {:short-info "Browse ticket in browser, admitting defeat at the hands of Atlassian"
                                         :long-info "takes a ticket number"
                                         :fn com/launch-in-browser!}
                         :get-mermaid-gantt {:short-info "Prints summary in mermaid gantt chart format"
                                             :long-info "takes a ticket number"
                                             :fn com/print-mermaid-gantt!}
                         :last-month-issues  {:short-info "Last month issues"
                                           :long-info "Everything I have touched, sorted by day of month"
                                           :fn com/print-last-month-activity!}
                         ;; Aliases
                         :C :add-comment
                         :L :link-tickets
                         :M :last-month-issues
                         :T :add-labels
                         :b :browse-ticket
                         :c :get-comments
                         :d :set-log-level
                         :e :everything-i-have-touched
                         :g :get-mermaid-gantt
                         :l :issues-by-label
                         :m :my-open-issues
                         :t :get-ticket
                         }}))




(mount/defstate
  ^{:on-reload :noop}
  cli-state
  :start (some-> (mount/args) :cli cli)
  :stop #(log/info "stopping cli" %))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  :print {:fn (fn [arg & opt-args]
                (print "Arg-type:" (type arg) "Arg: ")
                (pprint/pprint arg)
                (print "Opt-args: ")
                (pprint/pprint opt-args))
          :short-info "Pretty print the supplied arguments."
          :long-info "This function pretty prints its supplied arguments. It takes at least one argument."}
  

  )

(ns jira-cli.utils
  (:require
   [clojure.string :as str]
   [clojure.test :as test]
   [utilza.log :as ulog]
   [taoensso.timbre :as log]))

(defn everything-running?
  "Takes a coll of mount states. Returns true if all the services passd in are running"
  [states]
  (every? identity (map map? states)))


(defn fix-ticket
  "Takes a ticket string, regexp-mangles it,
  and tries to return it coerced into JIRA-000 format"
  [s]
  (let [[_ t n] (re-matches #"(\w+?)[-_]*(\d+)" (str s))]
    (str (str/upper-case t) "-" n)))


(test/deftest test-fix-ticket
  (->> ["OI-42" "oi-42" "oi42" "OI_42" "OI42"]
       (map fix-ticket)
       (map #(= "OI-42" %))
       (every? identity)
       test/is))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (test/run-tests))
  


  
  )

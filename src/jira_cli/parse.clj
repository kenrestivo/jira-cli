(ns jira-cli.parse
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [utilza.print-table :as print-table]
            [clojure.string :as str]
            [utilza.log :as ulog]
            [clj-time.format :as f]
            [utilza.misc :as umisc]
            [clj-http.util :as cutil]
            [clj-time.core :as t]
            [utilza.repl :as urepl]))

(def in-format (f/formatters :date-time))
(def out-format (f/formatters :date))
(def local-formatter (f/with-zone (f/formatter :mysql)
                       (t/default-time-zone)))


(defn localize-date
  "Takes a date string in the weird yyyy-MM-dd'T'HH:mm:ss.SSSSZ format jira uses.
  Returns a string formatted in sql date format"
  [s]
  (some->> s
       (f/parse in-format)
       (f/unparse local-formatter )))

(defn fix-dates
  [m]
  (umisc/munge-columns {:resolutiondate localize-date
                        :updated localize-date
                        :created localize-date}
                       m))


(defn readable-comment
  [{:keys [author body updated]}]
  {:author (:displayName author)
   :date (localize-date updated)
   :comment body}
  )

(defn readable-comments
  [{:keys [fields]}]
  (->> fields
       :comment
       :comments ;; XXX paging
       (map readable-comment)
       (sort-by :date)))


(defn readable-summary
  [{:keys [fields key]}]
  (let [{:keys [summary priority issuetype]} fields]
    (merge (select-keys  fields [:description :summary :labels])
           {:id key
            :pri (:name priority)
            :type (:name issuetype)})))



(defn get-subtasks
  [{{:keys [subtasks]} :fields}]
  (map :key subtasks))


(defn ->org-mode
  "Takes a seq of maps, returns org-mode-capable string"
  [ms]
  (->> ms
       (map fix-dates)
       print-table/printtable
       with-out-str))


(defn date-summarize-issue
  "Takes an issue map, returns a clojure map with id, pri, summary"
  [{:keys [key fields]}]
  (let [{:keys [summary updated]} fields]
    {:id key
     :updated updated
     :summary summary}))


(defn summarize-issue
  "Takes an issue map, returns a clojure map with id, pri, summary"
  [{:keys [key fields]}]
  (let [{:keys [summary priority issuetype]} fields]
    {:id key
     :pri (:name priority)
     ;;:type (:name issuetype) ;; seems empty these days
     :summary summary}))

(defn results->org-mode
  "Takes body of jql results, returns an org-mode-capable string"
  [issues]
  (->> issues
       (map fix-dates)
       (map summarize-issue)
       (sort-by :pri)
       ->org-mode))


(defn results->date-org-mode
  "Takes body of jql results, returns an org-mode-capable string"
  [issues]
  (->> issues
       (map date-summarize-issue)
       ->org-mode))

(defn select-as
  [m as ks]
  {as (get-in m ks)})


(defn simplify-date
  [s]
  (some->> s
           (f/parse in-format )
           (f/unparse  out-format)))

(defn fix-gantt
  [m]
  (umisc/munge-columns {:resolutiondate simplify-date
                        :updated simplify-date
                        :summary #(-> %
                                      (str/replace ":" "-" )
                                      (str/replace "," "-" ))
                        :created simplify-date}
                       m))

(defn status-fields
  [{:keys [fields]}]
  (merge (select-keys fields [:resolutiondate :updated  :created])
         (select-as fields :reporter [:reporter :displayName])))

(defn mermaid-gantt-fields
  [res]
  (let [{:keys [resolutiondate] :as m} (status-fields res)]
    (-> m
        (merge 
         (readable-summary res)
         ;; needs improvement, for in process
         {:status (when resolutiondate :done)})
        fix-gantt)))


(defn format-gantt-fields
  [{:keys [summary id resolutiondate status created]}]
  ;; JEDI-7937 - HLD for homegrown ssh key management/rotation system for edge :done,  2018-02-08, 2018-04-05
  (format "%s - %s :%s%s %s, %s"
          id
          summary
          (if status (name status) "")
          (if status "," "")
          created
          (if resolutiondate resolutiondate "")))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  )

(ns jira-cli.core
  (:require
   [mount.core :as mount]
   [jira-cli.log :as log]
   [jira-cli.nrepl :as nrepl]
   [jira-cli.net.cookies :as cookies]
   [jira-cli.cli :as cli]
   [jira-cli.utils :as utils]
   [io.aviso.repl :as pst]
   [utilza.log :as ulog])
  (:gen-class))

(defn start!
  [conf-file-arg]
  ;; TODO: handle conf file
  (mount/start-with-args {:cli {}
                          :log {:level :info}
                          :nrepl {:port 7778}}))



(defn -main
  [& [conf-file-arg & args]]
  (try
    (start! conf-file-arg )
    (System/exit 0)
    (catch Exception e
      ;; TODO: usage
      (pst/pretty-pst e)
      (println (.getMessage e))
      (println (.getCause e))
      (System/exit 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment
  
  (ulog/catcher
   (start! nil))

  (mount/reset)

  (mount/start #'jira-cli.net.cookies/cookies-state)
  
  (mount/stop-except #'jira-cli.nrepl/nrepl-state)

  (mount/start-except #'jira-cli.nrepl/nrepl-state)

  (future (ulog/catcher
           (mount/start-with-args {:cli {}
                                   :log {:level :info}
                                   })))
  )

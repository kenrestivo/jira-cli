(ns jira-cli.encode
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clojure.pprint :as pprint]
            [utilza.log :as ulog]
            [jira-cli.utils :as u]
            [clj-http.util :as cutil]
            [utilza.repl :as urepl]))


;; TODO; move this to conf as every jira install is different!!
(def custom-fields
  {:epic :customfield_10006 })


(defn add-labels
  [labels]
  {:update {:labels (for [l labels]
                      {:add l})}})

(defn link-issue
  [ticket link-and-type]
  {:update {:issuelinks (for [[ticket type] link-and-type]
                          {:add {:type {:name type}
                                 :inwardIssue {:key ticket}}})}})


(defn epics
  [ticket]
  {:fields ""
   :maxResults 2000
   :jql (format "\"Epic Link\" = %s"
                ticket)})


(defn everything-i-have-touched
  []
  {:fields "summary,updated"
   :maxResults 2000
   :jql "status changed by currentuser() ORDER BY updated DESC"})



(defn my-open-issues
  []
  {:fields "summary,priority"
   :maxResults 2000
   :jql "assignee = currentUser() AND resolution = Unresolved order by updated DESC"})



(defn by-label
  [label]
  {:fields "summary,priority"
   :maxResults 2000
   :jql (format "labels=\"%s\"" label)})


(defn simple-jql
  [jql]
  {:fields "summary,priority,issuetype"
   :maxResults 2000
   :jql jql})


(defn set-epic
  [ticket]
  {:fields {(:epic custom-fields) (u/fix-ticket ticket)}})

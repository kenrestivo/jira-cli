(ns jira-cli.commands
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            (cli4clj [cli :as cli])
            (clojure [pprint :as pprint])
            [mount.core :as mount]
            [jira-cli.net.actions :as actions]
            [clojure.string :as str]
            [me.raynes.conch.low-level :as sh]
            [jira-cli.net.queries :as queries]
            [jira-cli.parse :as parse]
            [jira-cli.utils :as utils]
            [jira-cli.encode :as enc]
            [jira-cli.conf :as conf]
            [utilza.misc :as umisc]
            [clj-http.client :as client]
            [clojure.pprint :as pprint]
            [utilza.log :as ulog]
            [utilza.repl :as urepl]))


(defn link-tickets!
  [to from reason]
  (log/debug "linking" to from reason)
  (actions/add-links! (conf/get-creds) (utils/fix-ticket to) [[(utils/fix-ticket from) (str/capitalize reason)]]))

(defn print-comments!
  [ticket-num]
  (doseq [{:keys [author date comment]} (->> ticket-num
                                             utils/fix-ticket
                                             (queries/comments (conf/get-creds)))]
    (println)
    (println "date: " date " author: " author)
    (println comment)))


(defn add-labels!
  [ticket & labels]
  (actions/add-labels! (conf/get-creds) (utils/fix-ticket ticket) labels))

(defn add-comment!
  [ticket & comment-words]
  (actions/add-comment! (conf/get-creds) (utils/fix-ticket ticket) (str/join " " comment-words)))



(defn pretty-print-issues-in-epic!
  [issues]
  (doseq [{:keys [summary id]} issues]
    (println id ": " summary)))

(defn pretty-print-ticket!
  [{:keys [id pri type description summary labels issues-in-epic subtasks
           created updated resolutiondate reporter]}]
  (println "id: " id " type: " type " pri " pri)
  (println "summary: " summary)
  (println "description: " description)
  (println "created: " created " by: " reporter)
  (println "updated: " updated)
  (println "closed:" resolutiondate)
  (println "labels: " labels)
  (println "issues in epic: ")
  (pretty-print-issues-in-epic! issues-in-epic)
  (println "subtasks: ")
  (pretty-print-issues-in-epic! subtasks))



(defn print-ticket!
  [ticket-num]
  (->> ticket-num
       utils/fix-ticket
       (queries/short-ticket-summary (conf/get-creds))
       pretty-print-ticket!))



(defn print-tickets-by-label!
  [label]
  (->> label
       enc/by-label
       (queries/get-generic-jql (conf/get-creds))
       parse/results->org-mode
       println))

(defn print-my-open-issues!
  []
  (->> (conf/get-creds)
      ;;; TODO: use the formatting tools built in to clj and cli4clj or whatever, there was soemthing cool there
       queries/get-my-open-issues
       println))


(defn print-everything-i-have-touched!
  []
  (->> (conf/get-creds)
      ;;; TODO: use the formatting tools built in to clj and cli4clj or whatever, there was soemthing cool there
       queries/get-everything-i-have-touched
       println))

(defn print-mermaid-gantt!
  [ticket]
  (->> ticket
       utils/fix-ticket
       (queries/get-ticket (conf/get-creds))
       parse/mermaid-gantt-fields
       parse/format-gantt-fields
       println))



(defn launch-in-browser!
  "WHen u just have to admpit defeat :("
  [ticket]
  (let [{:keys [url]} (conf/get-creds)]
    (sh/proc "x-www-browser" (str  url "/browse/" (utils/fix-ticket ticket)))))


(defn set-log-level!
  "set the log level"
  [level]
  (log/set-level! (keyword level)))


(defn print-last-month-activity!
  []
   ;; Tickets touched by me in last month, grouped by date
   ;; TODO: untangle this bolus into a function that can be run from CLI
   (doseq [[date id-summaries] (->> (reduce (fn [acc {:keys [date summary id]}]
                                    (update-in acc [date] (if (nil? (acc date))  vector conj ) (format "%s: %s" id summary)))
                                  {}
                                  (for [{:keys [key fields]} (->> (enc/everything-i-have-touched)
                                                                  (queries/get-generic-jql (conf/get-creds)))
                                        :let [{:keys [summary updated]} fields]]
                                    {:date (-> updated
                                               parse/localize-date
                                               (str/split #" ") ;; filthy but effective!
                                               first)
                                     :summary summary
                                     :id key}))
                          vec
                          (sort-by first)
                          reverse
                          (take 25))]
     (println (format "\n%s:\n%s\n" date
                      (str/join "\n" (filter identity id-summaries))))))

(ns jira-cli.conf
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.client :as client]
            [clojure.pprint :as pprint]
            [utilza.log :as ulog]
            [utilza.repl :as urepl]))

(defn get-creds
  ([path]
   (->> path
        slurp
        edn/read-string))
  ([]
   (get-creds (str (get (urepl/env) "HOME") "/.jira-creds.edn" ))))

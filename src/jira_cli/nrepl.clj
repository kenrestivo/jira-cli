(ns jira-cli.nrepl
  (:require
   [clojure.tools.nrepl.server :as nrepl]
   [mount.core :as mount]
   [taoensso.timbre :as log]))


(defn start-nrepl
  [settings]
  (log/info "starting nrepl connection" settings)
  (apply nrepl/start-server (apply concat settings)))

(mount/defstate
  ^{:on-reload :noop}
  nrepl-state
  :start (some->  (mount/args) :nrepl start-nrepl)
  :stop #(-> %  nrepl/stop-server future deref))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  )


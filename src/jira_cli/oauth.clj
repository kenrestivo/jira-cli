(ns jira-cli.oauth
  (:refer-clojure :exclude [replace])
  (:require
   [clj-http.client :refer [ wrap-url]]
   [clojure.java.browse :refer [browse-url]]
   [clojure.string :as str]
   [oauth.io :refer [request]]
   [oauth.util :as util]
   [oauth.v1 :as v1]
   )
  (:import
   com.google.api.client.auth.oauth.OAuthRsaSigner
   com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
   java.security.KeyFactory
   java.security.spec.PKCS8EncodedKeySpec
   ))


;; Via https://developer.atlassian.com/server/jira/platform/oauth/
;; and https://oauth.net/core/1.0a/
;;; also interesting https://developer.atlassian.com/cloud/jira/platform/jira-rest-api-oauth-authentication/


;; (def ^:dynamic *oauth-access-token-url*
;;   "https://api.twitter.com/oauth/access_token")

;; (def ^:dynamic *oauth-authentication-url*
;;   "https://api.twitter.com/oauth/authenticate")

;; (def ^:dynamic *oauth-authorization-url*
;;   "https://api.twitter.com/oauth/authorize")

;; (def ^:dynamic *oauth-request-token-url*
;;   "https://api.twitter.com/oauth/request_token")


(def ^:dynamic *oauth-version* "1.0a")


(defn unfuck-pem
  [s]
  (->> s
       str/split-lines
       (remove #(.contains % "---"))
       (apply str)))

(defn gen-private-key
  [keypath]
  ;; TODO: memoize maybe?
  (.generatePrivate (KeyFactory/getInstance "RSA")
                    (-> keypath
                        slurp
                        unfuck-pem
                        Base64/decodeBase64
                        PKCS8EncodedKeySpec.)))

(defn gen-signer
  [keypath]
  ;; TODO: memoize maybe?
  (let [signer (OAuthRsaSigner.)]
    (set! (. signer -privateKey) (gen-private-key keypath))
    signer))

(defn sign
  "Take a string s and path to a pem encoded private key.
  Sign the string and return it."
  [s keypath]
  (-> keypath
      gen-signer
      (.computeSignature  s)))



;; copied from oauth-clj


(defn oauth-request-signature
  "Calculates the OAuth signature from `request`."
  [request path-to-key]
  (sign (v1/oauth-signature-base request) path-to-key))

(defn oauth-sign-request
  "Sign the OAuth request with `consumer-key` and `token-secret`."
  [request path-to-key ]
  (let [signature (oauth-request-signature request path-to-key)]
    (assoc request :oauth-signature signature)))


(defn wrap-oauth-signature
  "Returns a HTTP client that signs an OAuth request."
  [client {:keys [oauth-consumer-secret]}]
  (fn [request]
    (client (oauth-sign-request request oauth-consumer-secret))))


(defn wrap-oauth-defaults
  "Returns a HTTP client with OAuth"
  [client & [params]]
  (fn [request]
    (->> {:oauth-nonce (v1/oauth-nonce)
          :oauth-signature-method "RSA-SHA1"
          :oauth-timestamp (str (v1/oauth-timestamp))
          }
         (merge params request)
         client)))


(defn make-consumer
  "Returns an OAuth consumer HTTP client."
  [& {:as oauth-defaults}]
  (-> request
      v1/wrap-remove-oauth-token
      ;; (util/wrap-content-type util/x-www-form-urlencoded) this breaks things
      v1/wrap-oauth-authorization
      (wrap-oauth-signature oauth-defaults)
      wrap-url
      (wrap-oauth-defaults oauth-defaults)))

(defn oauth-client
  "Returns a HTTP client for version 1 of the OAuth protocol."
  [oauth-token  oauth-token-secret consumer-key path-to-key]
  (make-consumer
   :oauth-token oauth-token
   :oauth-consumer-key consumer-key
   :oauth-consumer-secret path-to-key
   :oauth-verifier oauth-token-secret))

(defn client-from-conf
  [{:keys [token verifier consumer-key path-to-key]}]
  (make-consumer
   :oauth-token token
   :oauth-consumer-key consumer-key
   :oauth-consumer-secret path-to-key
   :oauth-verifier verifier)
  )

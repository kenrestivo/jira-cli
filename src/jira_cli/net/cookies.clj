(ns jira-cli.net.cookies
  (:require
   [mount.core :as mount]
   [clj-http.cookies :as cookies]
   [utilza.log :as ulog]
   [taoensso.timbre :as log]))

(defn start-cookies
  []
  (log/info "setting up cookie store")
  (cookies/cookie-store))

(mount/defstate
  ^{:on-reload :noop}
  cookies-state
  :start (start-cookies)
  :stop #(log/info "stopping cookie store" %))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/spewer
   (cookies/get-cookies cookies-state))
  
  )







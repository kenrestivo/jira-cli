(ns jira-cli.net.actions
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.client :as client]
            [jira-cli.conf :as conf]
            [jira-cli.net.queries :as queries]
            [jira-cli.oauth :as oauth]
            [jira-cli.net.cookies :as cookies]
            [jira-cli.encode :as enc]
            [jira-cli.parse :as parse]
            [clojure.pprint :as pprint]
            [utilza.log :as ulog]
            [clj-http.util :as cutil]
            [utilza.repl :as urepl]))


(defn add-labels!
  "Takes creds, a ticket string, and a coll of label strings. 
   Side-effecting, adds the labels to the ticket"
  [[url & creds] ticket labels]
  ((oauth/client-from-conf creds) {:url (format "%s/rest/api/2/issue/%s"
                                                url
                                                ticket)
                                   :method :put
                                   :as :json
                                   :form-params (enc/add-labels labels)
                                   :cookie-policy :standard
                                   :cookie-store cookies/cookies-state
                                   :content-type :json
                                   :headers {"Content-Type" "application/json"}}))


(defn add-labels-subtasks!
  "Takes creds, a ticket string, and a coll of label strings. 
   Side-effecting, adds the labels to all the subtasks of the ticket"
  [[url & creds] ticket labels]
  (doseq [ticket (->> ((oauth/client-from-conf creds) 
                       {:url (format "%/rest/api/2/issue/%s"
                                     url
                                     ticket)
                        :method :get
                        :as :json
                        :cookie-policy :standard
                        :cookie-store cookies/cookies-state
                        :headers {"Content-Type" "application/json"}})
                      parse/get-subtasks)]
    ;; TODO: debug logging
    (add-labels! creds ticket labels)))



(defn add-labels-epic-tickets!
  "Takes creds, a ticket string of an epic, and a coll of label strings. 
   Side-effecting, adds the labels to all the tickets of the epic"
  [[url & creds] ticket labels]
  (doseq [ticket (queries/tickets-in-epic creds ticket)]
    ;; TODO; debug logging
    (add-labels! ticket labels)))


(defn add-links!
  "Takes creds, a ticket string, and a coll of pairs of [ticket type-of-link].
   Side-effecting, adds links of that type for all of the links-and-types"
  [[url & creds] ticket links-and-types]
  ((oauth/client-from-conf creds) {:url (format "%s/rest/api/2/issue/%s"
                                                url
                                                ticket)
                                   :method :put
                                   :as :json
                                   :form-params (enc/link-issue ticket links-and-types)
                                   :cookie-policy :standard
                                   :cookie-store cookies/cookies-state
                                   :content-type :json
                                   :headers {"Content-Type" "application/json"}
                                   }))



(defn add-comment!
  "Takes creds, a ticket string, and a commen.
   Side-effecting, adds the comment to the ticket"
  [[url & creds] ticket comment]
  ((oauth/client-from-conf creds) {:url (format "%s/rest/api/2/issue/%s/comment"
                                                url
                                                ticket)
                                   :method :post
                                   :as :json
                                   :form-params {:body comment}
                                   :cookie-policy :standard
                                   :cookie-store cookies/cookies-state
                                   :content-type :json
                                   :headers {"Content-Type" "application/json"}
                                   }))



(defn set-epic!
  [[url & creds] ticket epic-ticket]
  ((oauth/client-from-conf creds) 
                   {:url (format "%s/rest/api/2/issue/%s"
                           url
                           ticket)
                    :method :put
                    :as :json
                    :form-params (enc/set-epic epic-ticket)
                    :cookie-policy :standard
                    :cookie-store cookies/cookies-state
                    :content-type :json
                    :headers {"Content-Type" "application/json"}
                    }))

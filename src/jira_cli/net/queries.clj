(ns jira-cli.net.queries
  (:require [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [clj-http.client :as client]
            [clj-time.format :as f]
            [jira-cli.encode :as enc]
            [jira-cli.oauth :as oauth]
            [jira-cli.net.cookies :as cookies]
            [jira-cli.parse :as parse]
            [utilza.misc :as umisc]
            [clojure.pprint :as pprint]
            [utilza.log :as ulog]
            [clj-http.util :as cutil]
            [utilza.repl :as urepl]))





(defn get-generic-jql
  "Takes creds and a map with the formatted JQL query, returns the issues.
   Paginates and will make multiple REST calls to get all the records. Could take a while."
  [{:keys [url] :as creds} jql-params]
  (loop [acc []
         p jql-params]
    (let [body ((oauth/client-from-conf creds) {:as :json
                                                :method :get
                                                :url (format "%s/rest/api/2/search" url)
                                                :cookie-store cookies/cookies-state
                                                :cookie-policy :standard
                                                :query-params p})
          {:keys [start-at total max-results issues]} body
          pos (+ start-at max-results)
          new-acc (concat acc issues)]
      (log/debug "starting" start-at
                 "of" total)
      (if (> total pos)
        (recur new-acc  (assoc jql-params :startAt pos))
        ;; TODO: pass total up for display
        new-acc))))


(defn linked-ticket-summary
  [{:keys [fields key]}]
  {:summary (:summary fields)
   :id key})


(defn tickets-in-epic
  "Takes creds and a ticket string, returns the list of tickets that are in the epic"
  [creds ticket]
  (->> ticket
       enc/epics
       (get-generic-jql creds)
       (map linked-ticket-summary)
       (sort-by :id)))


(defn get-everything-i-have-touched
  "Takes creds, returns the cred's open issues in an org-mode-compatible string"
  [creds]
  (->> (enc/everything-i-have-touched)
       (get-generic-jql creds)
       parse/results->date-org-mode))


(defn get-my-open-issues
  "Takes creds, returns the cred's open issues in an org-mode-compatible string"
  [creds]
  (->> (enc/my-open-issues)
       (get-generic-jql creds)
       parse/results->org-mode))



(defn parse-su
  [{:keys [fields] :as m}]
  (-> m
      parse/readable-summary
      (assoc :subtasks (parse/get-subtasks m))))


(defn get-ticket
  [{:keys [url] :as creds} ticket]
  ((oauth/client-from-conf creds)  {:url (format "%s/rest/api/2/issue/%s"
                                                 url
                                                 ticket)
                                    :method :get
                                    :as :json
                                    :cookie-policy :standard
                                    :cookie-store cookies/cookies-state
                                    :headers {"Content-Type" "application/json"}
                                    }))

(defn short-ticket-summary
  [creds ticket]
  (let [{:keys [key fields] :as m} (get-ticket creds ticket)]
    ;; NOTE: ticket key may have changed (if it was moved between projects)
    (->> m
         parse/readable-summary
         (merge {:subtasks (map linked-ticket-summary (:subtasks fields))})
         (merge (parse/status-fields m))
         (merge {:issues-in-epic (tickets-in-epic creds key)})
         parse/fix-dates)))



(defn ticket-summary
  "LONG"
  [creds ticket]
  (->> (get-ticket creds ticket)
       ;; TODO: needs improvement
       (cons (tickets-in-epic creds ticket))))




(defn comments
  [creds ticket]
  (->> (get-ticket creds ticket)
       ;; TODO: needs improvement
       parse/readable-comments))






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  
  (ulog/catcher
   (f/parse in-format "2017-11-28T11:34:46.411-0800" ))


  (ulog/catcher
   (simplify-date "2017-11-28T11:34:46.411-0800"))

  (f/show-formatters)
  


  )

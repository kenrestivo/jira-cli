(ns jira
  (:require
   [cheshire.core :as json]
   [clj-http.client :as client]
   [clj-http.util :as cutil]
   [clojure.edn :as edn]
   [jira-cli.oauth :as jo]
   [clojure.java.browse :as browse]
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [clj-time.format :as f]
   [clj-time.local :as l]
   [clj-time.core :as t]
   [jira-cli.conf :as conf]
   [jira-cli.encode :as enc]
   [jira-cli.net.actions :as actions]
   [jira-cli.net.queries :as queries]
   [jira-cli.commands :as commands]
   [jira-cli.net.cookies :as cookies]
   [jira-cli.oauth :as oauth]
   [jira-cli.parse :as parse]            
   [taoensso.timbre :as log]
   [utilza.log :as ulog]
   [utilza.misc :as umisc]
   [utilza.repl :as urepl]
   )
  (:import
   com.atlassian.oauth.client.example.JiraOAuthClient
   com.atlassian.oauth.client.example.OAuthClient
   com.atlassian.oauth.client.example.PropertiesClient
   com.google.api.client.auth.oauth.OAuthAuthorizeTemporaryTokenUrl
   com.google.api.client.auth.oauth.OAuthGetTemporaryToken
   com.google.api.client.auth.oauth.OAuthRsaSigner
   com.google.api.client.http.GenericUrl
   com.google.api.client.http.HttpRequest
   com.google.api.client.http.HttpRequestFactory
   com.google.api.client.http.apache.ApacheHttpTransport
   com.google.api.client.http.javanet.NetHttpTransport   
   com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
   java.security.KeyFactory
   java.security.spec.PKCS8EncodedKeySpec
   ))






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment



  (ulog/spewer
   (queries/ticket-summary (conf/get-creds) "JEDI-1897"))


  (do
    (log/info "wtf")
    (ulog/spewer
     (add-labels! "OI-70" ["CI-CD-SCRUM"
                           "Infrastructure"
                           "QIP17.2"
                           "QIP17.2-Candidate"
                           "SSOE"
                           "Security"])))

  (do
    (log/info "wtf")
    (ulog/spewer
     (actions/add-labels-subtasks! (conf/get-creds) "JEDI-11204"
                                   ["kinesis_historians"])))
  
  
  (ulog/catcher
   (spit "/tmp/issues.org" "")
   (->> (conf/get-creds)
        queries/get-my-open-issues
        (spit "/tmp/issues.org")))



  (ulog/catcher
   (spit "/tmp/issues.org" "")
   (->> "salesforce"
        enc/by-label
        (queries/get-generic-jql (conf/get-creds))
        parse/results->org-mode
        (spit "/tmp/issues.org")))
  

  (ulog/catcher
   (spit "/tmp/issues.org" "")
   (->> (conf/get-creds)
        queries/get-my-open-issues
        (spit "/tmp/issues.org")))
  

  (ulog/catcher
   (actions/add-links! (conf/get-creds) "OI-1866" [["JEDI-2223" "Relates"]]))


  )


(comment

  (ulog/catcher
   (->>"labels = CI-CD-SCRUM AND type = Epic AND status != Accepted AND status != \"Ready for Acceptance\" AND status != Done ORDER BY status ASC, updated DESC"
       enc/simple-jql
       (queries/get-generic-jql (conf/get-creds))
       parse/results->org-mode
       (spit "/tmp/issues.org"))


   )
  




  (actions/add-labels!
   (conf/get-creds)
   "OI-1523"
   ["Infrastructure"
    "QI172_scope_reduction_20170724"
    "QIP17.2"
    "QIP17.2_committed_epics"
    "SSOE"
    "candidate_qip17.2_sprint2"
    "candidate_qip17.2_sprint3"
    "ci-cd-scrum"
    "mysql"
    "s111"])



  
  )


(comment

  (def ticket "JEDI-10284")
  

  (- 1.293 1.461)


  (ulog/spewer
   (queries/short-ticket-summary (conf/get-creds) ticket))
  
  (ulog/spewer
   (queries/get-ticket (conf/get-creds) ticket))

  (ulog/spewer
   (queries/comments (conf/get-creds) ticket))
  
  (ulog/spewer
   (queries/status (conf/get-creds) ticket))

  (ulog/spewer
   (->> ticket
        (queries/get-ticket (conf/get-creds))
        keys))

  (ulog/catcher
   (->> ticket
        (queries/get-ticket (conf/get-creds))
        parse/mermaid-gantt-fields
        parse/format-gantt-fields))  


  (def label "elk")

  (log/set-level! :info)
  
  (ulog/spewer
   (->> label
        enc/by-label
        (queries/get-generic-jql (conf/get-creds))))
  

  
  
  (future
    ;; hack for generating gantt charts from jira labels :)
    (ulog/catcher
     (let [label "vault"]
       (doseq [{:keys [id]} (->> label
                                 enc/by-label
                                 (queries/get-generic-jql (conf/get-creds))
                                 :issues)]
         (->> id
              (queries/get-ticket (conf/get-creds))
              parse/mermaid-gantt-fields
              parse/format-gantt-fields
              println))))) 


  
  )

(comment

  ;; Build a nice tree
  (ulog/spewer
   (:acc (reduce (fn [{:keys [acc current] :as m} l]
                   (if (.startsWith l "section")
                     (assoc m :current l)
                     (update-in m [:acc current] conj (some-> l (str/split  #" ") first))))
                 {:current nil
                  :acc {}}
                 (umisc/read-lines "/home/src/vault-utils/doc/schedule.mermaid"))))

  )


(comment

  (ulog/spewer
   (->> (enc/my-open-issues)
        (queries/get-generic-jql (conf/get-creds))))


  )


(comment

  (ulog/catcher
   (actions/set-epic! (conf/get-creds) ticket   ""))


  (ulog/spewer
   (queries/tickets-in-epic (conf/get-creds) ticket))


  (log/set-level! :trace)

  
  (ulog/spewer
   (->> (enc/everything-i-have-touched)
        (queries/get-generic-jql (conf/get-creds))))



  




  )


(comment

  (ulog/catcher
   (->> (queries/get-everything-i-have-touched (conf/get-creds))
        (spit "/tmp/issues.org")))

  )



(comment

  ;; (ulog/spewer
  ;;  ;; works to obtain a token, if you use the hacked library
  ;;  (let [{:keys [url token verifier consumer-key path-to-key] (conf/get-creds)}
  ;;        client (JiraOAuthClient. (PropertiesClient.))
  ;;        resp (.getAndAuthorizeTemporaryToken  client
  ;;                                              consumer-key
  ;;                                              (-> path-to-key
  ;;                                                  slurp
  ;;                                                  oauth/unfuck-pem))
  ;;        url (OAuthAuthorizeTemporaryTokenUrl. (.authorizationUrl client))]
  ;;    (set! (. url  -temporaryToken) (.token resp))
  ;;    {:token (.token resp)
  ;;     :secret (.tokenSecret resp)
  ;;     :url (.toString url)}))

  
  (browse/browse-url (:url *1))

  
  ;; (ulog/catcher
  ;;  ;; using modded example code, works!!
  ;;  (let [{:keys [url token verifier consumer-key path-to-key] (conf/get-creds)}
  ;;        client (JiraOAuthClient. (PropertiesClient.))]
  ;;    (.getAccessToken client
  ;;                     token ;; the ONE-TIME REQUEST TOKEN (not the secret! the secret appears to be unused!)
  ;;                     verifier ;; the KEY THAT COMES FROM JIRA UI after running the request temporary step
  ;;                     consumer-key ;; "consumer key" NOT A KEY
  ;;                     (-> path-to-key
  ;;                         slurp
  ;;                         oauth/unfuck-pem))))

  ;; token secret is apparently UNUSED ANYWHERE 
  
  ;; (ulog/spewer
  ;;  ;; this works, but wtf is this thing actually doing under the hood?
  ;;  (let [{:keys [url token verifier consumer-key path-to-key] (conf/get-creds)}
  ;;        client (JiraOAuthClient. (PropertiesClient.))
  ;;        params (.getParameters client
  ;;                               token ;; the ACCESS TOKEN oauth_token
  ;;                               verifier ;; the validation KEY THAT COMES FROM JIRA UI oauth_verifier
  ;;                               consumer
  ;;                               (-> path-to-key
  ;;                                   slurp
  ;;                                   oauth/unfuck-pem))]
  ;;    (-> (NetHttpTransport.)
  ;;        (.createRequestFactory  params)
  ;;        (.buildGetRequest (GenericUrl. (str url "/rest/api/latest/project")
  ;;                                       ;;"http://localhost:3000/rest/api/latest/project"
  ;;                                       ))  ;; 
  ;;        .execute ;; returns a response
  ;;        .getContent
  ;;        .readAllBytes
  ;;        String.
  ;;        (json/decode true))))

  

  (ulog/catcher
   (.getStatusCode resp))

  
  (.setLoggingEnabled foo true)
  
  (ulog/catcher (/ 1 0))
  
  (def foo *1)
  (urepl/hjall *1)

  
  (ulog/catcher
   ;; works
   (jo/sign "hey wtf" (:path-to-key (conf/get-creds))))

  (ulog/spewer :wtf)




  
  )


(comment

  ;; memoize?
  
  

  (def client (oauth/client-from-conf (conf/get-creds)))

  (log/set-level! :trace)

  
  (log/error :wtf)

  
  (ulog/catcher
   (commands/launch-in-browser! "dops1084"))


  (ulog/spewer
   ;; works!
   (-> (queries/get-ticket (conf/get-creds) "DOPS-1441")
                                        ;parse/readable-summary
       ))
  

  (ulog/spewer
   (->> "bazel/buildkite"
        enc/by-label
        (queries/get-generic-jql (conf/get-creds))
        
        ))

  (ulog/spewer
   (for [f [codec/url-encode codec/form-encode codec/percent-encode] ]
     (f "bazel/buildkite")))


  
  
  (ulog/catcher
   (f/parse parse/in-format "2020-12-04T00:30:03.898+0000"))




  (f/show-formatters)


  (ulog/catcher
   (parse/localize-date "2020-12-04T00:30:03.898+0000"))

  (ulog/catcher
   (commands/print-ticket! "DOPS-1362" ))

  (ulog/spewer
   (->> "DOPS-1362"
        (queries/short-ticket-summary (conf/get-creds))
        ;;   parse/fix-dates ;; XXX this feels like the absolute wrong place for this
        ;;commands/pretty-print-ticket!
        ))
  
  )

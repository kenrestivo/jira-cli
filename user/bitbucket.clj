(ns bitbucket
  (:require
   [utilza.log :as ulog]
   [clojure.string :as str]
   [utilza.repl :as urepl]
   [clojure.string :as str]
   [clojure.walk :as walk]
   [cheshire.core :as json]
   [jira-cli.conf :as conf]
   [jira-cli.net.queries :as queries]
   [clj-yaml.core :as yaml]
   [jira-cli.net.actions :as actions]
   [jira-cli.bitbucket :as bb]
   [jira-cli.jenkins-job-builder :as jjb]
   [jira-cli.encode :as enc]
   [jira-cli.parse :as parse]            
   [clojure.pprint :as pprint]
   [clojure.edn :as edn]
   [me.raynes.conch.low-level :as sh]
   [utilza.file :as f]
   [utilza.misc :as umisc]
   [clj-http.client :as client]
   [utilza.core :as utilza]
   [taoensso.timbre :as log]
   [camel-snake-kebab.core :as csk]
   ))






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment




  (ulog/spewer
   (bb/get-all-repos (conf/get-creds) "my-company"))
  


  (ulog/spewer
   (bb/get-all-repos-and-deploy-keys (conf/get-creds) "my-company"))



  (->> "/home/work/logs/all-bitbucket.edn"
       slurp
       edn/read-string
       json/encode
       (spit "/tmp/foo.json"))


  (log/set-level! :trace)

  
  )


(comment


  (bb/add-key-to-repos! (conf/get-creds) "/path/to-jenkins-jobs" "accountname" "SSH key string" "name-of-key")

  
  )

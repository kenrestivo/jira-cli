# jira-cli

CLI and/or REST tools for doing common and bulk operations on JIRA without getting stuck in clicky-draggy and RSI hell.

## Build and Installation

```sh
lein uberjar

```
Will put the resulting jar in  target/uberjar/jira-cli.jar

Then copy that wherever.

## Usage

Put a file in ~/.jira-creds.edn

```clojure
["https://your-jira-url" "your.email@address.com" "password"]

```

Then run

```sh
java -jar jira-cli.jar
```

## Examples

```
jira> h
h
add-comment [C] -- Arguments: [[ticket & comment-words]]
	Add comment to tickets
	Takes a ticket number, and the comment

add-labels [T] -- Arguments: [[ticket & labels]]
	Add labels to tickets
	Takes a ticket number, and any number of labels

get-comments [c] -- Arguments: [[ticket-num]]
	Get comments about a ticket
	takes a ticket number

get-ticket [t] -- Arguments: [[ticket-num]]
	Get info about a ticket
	takes a ticket number

help [? h]
	Show help.
	Display a help text that lists all available commands including further detailed information about these commands.

issues-by-label [l] -- Arguments: [[label]]
	Print all issues by label
	Takes a label.

link-tickets [L] -- Arguments: [[to from reason]]
	Link 2 tickets
	Takes a ticket number, ticket number to link to, and reason

my-open-issues [m] -- Arguments: [[]]
	Get my open issues
	prints them on org mode

quit [q]
	Quit the CLI.
	Terminate and close the command line interface.

test
	Test Command
	Prints a test message to stdout.

jira>

```

There is more example usage in user/jira.clj


### Bugs

LOL WIP.

## License

Copyright © 2017-2020 ken restivo

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
